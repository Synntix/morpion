use read_input::prelude::*;

struct Joueur {
    num: u8,
    caractere: char,
    nom: String,
}
impl Joueur {
    fn new(num: u8) -> Joueur {
        let nom = input::<String>()
            .msg(format_args!("Nom du joueur {}: ", num))
            .get();

        let caractere = input::<char>()
            .repeat_msg(format_args!("Caractère de {}: ", nom))
            .err("Ce n'est pas un caractère !")
            .get();

        Joueur {
            num,
            caractere,
            nom,
        }
    }
}

struct Jeu {
    grille: [char; 9],
    joueurs: [Joueur; 2],
    joueur_actif: u8,
}
impl Jeu {
    fn init() -> Jeu {
        let j1 = Joueur::new(1);
        println!();
        let j2 = Joueur::new(2);
        Jeu {
            grille: [' '; 9],
            joueurs: [j1, j2],
            joueur_actif: 0,
        }
    }

    fn _test() -> Jeu {
        let j1 = Joueur {
            num: 1,
            caractere: 'X',
            nom: String::new(),
        };
        let j2 = Joueur {
            num: 2,
            caractere: 'X',
            nom: String::new(),
        };

        let mut jeu = Jeu {
            grille: [' '; 9],
            joueurs: [j1, j2],
            joueur_actif: 1,
        };

        jeu.grille[0] = 'X';
        jeu.grille[1] = 'O';
        jeu.grille[2] = 'X';
        jeu.grille[3] = 'O';
        jeu.grille[4] = 'O';
        jeu.grille[5] = 'X';
        jeu.grille[6] = 'X';
        jeu.grille[7] = 'X';
        jeu.grille[8] = 'O';
        jeu
    }
}

fn affiche_grille(grille: &[char; 9]) {
    println!(" {} | {} | {}", grille[0], grille[1], grille[2]);
    println!("---+---+---");
    println!(" {} | {} | {}", grille[3], grille[4], grille[5]);
    println!("---+---+---");
    println!(" {} | {} | {}\n", grille[6], grille[7], grille[8]);
}

/// 1 si joueur 1 gagne
/// 2 si joueur 2 gagne
/// 3 si Egalité
/// 0 si rien
fn ifwin(jeu: &Jeu) -> u8 {
    let mut egal: u8 = 1;
    let grille = jeu.grille;

    for joueur in jeu.joueurs.iter() {
        //pour toute les lignes
        for i in (0..7).step_by(3) {
            if grille[i] == joueur.caractere
                && grille[i + 1] == joueur.caractere
                && grille[i + 2] == joueur.caractere
            {
                return joueur.num;
            }
        }

        //pour toute les colonnes
        for j in 0..4 {
            if grille[j] == joueur.caractere
                && grille[j + 3] == joueur.caractere
                && grille[j + 6] == joueur.caractere
            {
                return joueur.num;
            }
        }

        //pour les diagonales
        if grille[0] == joueur.caractere
            && grille[4] == joueur.caractere
            && grille[8] == joueur.caractere
        {
            return joueur.num;
        }

        if grille[2] == joueur.caractere
            && grille[4] == joueur.caractere
            && grille[6] == joueur.caractere
        {
            return joueur.num;
        }

        for i in 0..9 {
            if grille[i] == ' ' {
                egal = 0;
            }
        }
    }
    if egal == 1 {
        return 3;
    }

    return 0;
}

fn main() {
    let mut jeu = Jeu::init();

    //set up du jeu
    print!("=============================================================================\n\n");
    print!("  $$\\      $$\\                               $$\\                          \n");
    print!("  $$$\\    $$$ |                              \\__|                          \n");
    print!("  $$$$\\  $$$$ | $$$$$$\\   $$$$$$\\   $$$$$$\\  $$\\  $$$$$$\\  $$$$$$$\\   \n");
    print!("  $$\\$$\\$$ $$ |$$  __$$\\ $$  __$$\\ $$  __$$\\ $$ |$$  __$$\\ $$  __$$\\  \n");
    print!("  $$ \\$$$  $$ |$$ /  $$ |$$ |  \\__|$$ /  $$ |$$ |$$ /  $$ |$$ |  $$ |      \n");
    print!("  $$ |\\$  /$$ |$$ |  $$ |$$ |      $$ |  $$ |$$ |$$ |  $$ |$$ |  $$ |       \n");
    print!("  $$ | \\_/ $$ |\\$$$$$$  |$$ |      $$$$$$$  |$$ |\\$$$$$$  |$$ |  $$ |     \n");
    print!("  \\__|     \\__| \\______/ \\__|      $$  ____/ \\__| \\______/ \\__|  \\__|\n");
    print!("                                   $$ |                                      \n");
    print!("                                   $$ |                                      \n");
    print!("                                   \\__|                                     \n\n");
    print!("=============================================================================\n\n");

    while ifwin(&jeu) == 0 {
        affiche_grille(&jeu.grille);
        println!("Au tour de {} ! ", jeu.joueurs[jeu.joueur_actif as usize].nom);

        let mut user_input : u8 = 0;
        loop {
            user_input = input::<u8>()
            .repeat_msg("Entre un chiffre entre 1 et 9 pour choisir ta case : ")
            .inside_err(1..=9, "Le chiffre doit être compris entre 1 et 9")
            .get();

            if jeu.grille[(user_input-1) as usize] == ' ' {
                break;
            } else {
                println!("Cette case est déjà prise !");
            }
        }

        jeu.grille[(user_input-1) as usize] = jeu.joueurs[jeu.joueur_actif as usize].caractere;

        if jeu.joueur_actif == 0 {
            jeu.joueur_actif = 1;
        } else {
            jeu.joueur_actif = 0;
        }

    }

    affiche_grille(&jeu.grille);

    match ifwin(&jeu) {
        0 => println!("Whut ?"),
        3 => println!("Égalité !"),
        _ => println!("{} a gagné !", jeu.joueurs[(ifwin(&jeu)-1) as usize].nom),
    }

}
