/*
  Grille de jeu = tableau de 9 short
  case vide = 0
  case joueur 1 = 1
  case joueur 2 = 2
*/

#include <stdio.h>
#include <stdlib.h>

typedef short *t_grille;

char play1 = 'X';
char play2 = 'O';

/*void afficherCroix(){
  printf("    \\    /  \n");
  printf("     \\  /   \n");
  printf("      \\/    \n");
  printf("      /\\    \n");
  printf("     /  \\   \n");
  printf("    /    \\  \n");
}

void afficherRond(){
  printf("    *  *    \n");
  printf(" *        * \n");
  printf("*          *\n");
  printf("*          *\n");
  printf(" *        * \n");
  printf("    *  *    \n");
}

void afficherRien(){
  printf("            \n");
  printf("            \n");
  printf("            \n");
  printf("            \n");
  printf("            \n");
  printf("            \n");
}

void affihcerBarreV(){
  printf(" | \n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
}*/

char charPlayer(int player){
  if(player == 1) {return play1;}
  else if(player == 2) {return play2;}
  else {return ' ';}
}

int setCharPlayer(int player, char character){
  if(player == 1) {play1 = character; return 1;}
  else if(player == 2) {play2 = character; return 1;}
  else {return 0;}
}

void afficherGrille(t_grille grille){
  printf(" %c | %c | %c\n", charPlayer(grille[0]), charPlayer(grille[1]), charPlayer(grille[2]));
  printf("---+---+---\n");
  printf(" %c | %c | %c\n", charPlayer(grille[3]), charPlayer(grille[4]), charPlayer(grille[5]));
  printf("---+---+---\n");
  printf(" %c | %c | %c\n\n", charPlayer(grille[6]), charPlayer(grille[7]), charPlayer(grille[8]));

}



int ifVictoire(t_grille grille){

  int egal = 1;

  for (short joueur = 1; joueur <= 2; joueur++) {

    // pour tout les lignes
    for (size_t i = 0; i < 6; i += 3) {
      if(grille[i] == joueur && grille[i+1] == joueur && grille[i+2] == joueur) {return joueur;}
    }

    //pour toute les colonnes
    for (size_t j = 0; j < 3; j++) {
      if(grille[j] == joueur && grille[j+3] == joueur && grille[j+6] == joueur) {return joueur;}
    }

    //pour les diagonales
    if (grille[0] == joueur && grille[4] == joueur && grille[8] == joueur) {return joueur;}

    if (grille[2] == joueur && grille[4] == joueur && grille[6] == joueur) {return joueur;}
  }

  for (size_t i = 0; i < 9; i++) {
    if (grille[i] == 0) {egal = 0;}
  }

  if(egal == 1) {return 3;}

  return 0;
}



int remplir_case(short *grille, short joueur_actif, int input) {

  //Si l'input est incorrecte
  if (input < 1 || input > 9) {return 0;}

  //Si cette case est déjà remplie
  if (grille[input-1] != 0) {return 0;}

  grille[input-1] = joueur_actif;

  return 1;
}



void switch_joueur(short *joueur) {
  if (*joueur == 1) {
    *joueur = 2;
  } else {
    *joueur = 1;
  }
}



int prompt(int joueur, int erreur) {
  int input;
  if (erreur == 0) {printf("Joueur %d c'est à toi de jouer !\n", joueur);}
  else {printf("Joueur %d, tu n'a pas entrer un case valide !\n", joueur);}
  printf("Entre une case entre 1 et 9 pour choisir ta case : ");
  scanf("%i", &input);
  while ( getchar() != '\n' );

  return input;
}


int main(int argc, char const *argv[]) {

  short joueur_actif = 1;

  short *grille = malloc(sizeof(short)*9);
  for (size_t i = 0; i < 9; i++) {
    *grille = 0;
  }

  short input = 0;
  int error = 0;

  //set up du jeu
  printf("=============================================================================\n\n");
  printf("  $$\\      $$\\                               $$\\                          \n");
  printf("  $$$\\    $$$ |                              \\__|                          \n");
  printf("  $$$$\\  $$$$ | $$$$$$\\   $$$$$$\\   $$$$$$\\  $$\\  $$$$$$\\  $$$$$$$\\   \n");
  printf("  $$\\$$\\$$ $$ |$$  __$$\\ $$  __$$\\ $$  __$$\\ $$ |$$  __$$\\ $$  __$$\\  \n");
  printf("  $$ \\$$$  $$ |$$ /  $$ |$$ |  \\__|$$ /  $$ |$$ |$$ /  $$ |$$ |  $$ |      \n");
  printf("  $$ |\\$  /$$ |$$ |  $$ |$$ |      $$ |  $$ |$$ |$$ |  $$ |$$ |  $$ |       \n");
  printf("  $$ | \\_/ $$ |\\$$$$$$  |$$ |      $$$$$$$  |$$ |\\$$$$$$  |$$ |  $$ |     \n");
  printf("  \\__|     \\__| \\______/ \\__|      $$  ____/ \\__| \\______/ \\__|  \\__|\n");
  printf("                                   $$ |                                      \n");
  printf("                                   $$ |                                      \n");
  printf("                                   \\__|                                     \n\n");
  printf("=============================================================================\n\n");

  //choix du caractères
  char symbole = '\n';

  do {
    printf("Joueur 1, choisis ton caractère : ");
    symbole = getchar();
    while ( getchar() != '\n' );

    if (symbole == '\n' || symbole == ' ') {
      printf("Caractères invalide\n");
    }
  } while(symbole == '\n' || symbole == ' ');
  setCharPlayer(1, symbole);


  do {
    printf("Joueur 2, choisis ton caractère : ");
    symbole = getchar();
    while ( getchar() != '\n' );

    if (symbole == '\n' || symbole == ' ' || symbole == charPlayer(1)) {
      printf("Caractères invalide\n");
    }
  } while(symbole == '\n' || symbole == ' ' || symbole == charPlayer(1));
  setCharPlayer(2, symbole);


  printf("\n\nTrès bien ! Joueur 1 apparait en '%c' et joueur 2 apparait en '%c'\n\n", charPlayer(1), charPlayer(2));

  printf("===============================\n");
  printf(" _____       _           _   \n");
  printf("|  __ \\     | |         | |  \n");
  printf("| |  | | ___| |__  _   _| |_\n");
  printf("| |  | |/ _ \\ '_ \\| | | | __|\n");
  printf("| |__| |  __/ |_) | |_| | |_\n");
  printf("|_____/ \\___|_.__/ \\__,_|\\__|\n");
  printf("===============================\n");






  // Boucle de jeu
  while (ifVictoire(grille) == 0) {
    error = 0;
    afficherGrille(grille);
    do {
      input = prompt(joueur_actif,error);
      error = 1;
    } while(!remplir_case(grille, joueur_actif, input));
    switch_joueur(&joueur_actif);
    printf("\033[2J");
  }

  printf("=================\n");
  printf(" ______ _        \n");
  printf("|  ____(_)       \n");
  printf("| |__   _ _ __   \n");
  printf("|  __| | | '_ \\  \n");
  printf("| |    | | | | | \n");
  printf("|_|    |_|_| |_| \n");
  printf("=================\n\n");

  if(ifVictoire(grille) == 1 || ifVictoire(grille) == 2) {
    printf("Joueur %d gagne ! GG à lui ^^\n", ifVictoire(grille));
  }
  else if(ifVictoire(grille) == 3) {
    printf("Egalité ! Pas de bol. Vous pouvez toujours faire du jeté de chaise savoir qui est meilleur.\n");
  }


  printf("Voici la grille si vous voulez débrefier.\n\n");

  afficherGrille(grille);


  free(grille);

  return EXIT_SUCCESS;
}
